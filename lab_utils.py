from utils.csv_handling import *
from utils.numeric_math import *
import matplotlib.pyplot as plt
import numpy as np
import symbolic_calc as sym


c = 299792458 #m/s speed of light
color_list = ["red","yellow","green","blue"]
ratio = 4.135e-15

BaseSetting = {
    "limits":{
        "red":(-3,3),
        "yellow":(-3,3),
        "green":(-3,3),
        "blue":(-3,3),
        },
    "start_max_frac":0.5,
    "end_max_frac":0.5
    }
def get_frequency(wavelength,rel_wavelength):
    """
    wavelength in nm
    returns frequency and relative error of it
    """
    return c/wavelength*1e9,rel_wavelength

def get_wavelength(color):
    """
    data["grating"] is lines per meter

    returns wavelength in nanonmeter
    """
    data = get_data_fromsearchkey(f"{color}_wave")
    y,x=data["maxima_dist"], data["dist"]
    d_y,d_x=data["d_maxima_dist"], data["d_dist"]
    g= data["grating"]
    wavelength,rel_wavelength = sym.W_func(y,x,g),sym.rel_W_func(y,d_y,x,d_x,g)
    return wavelength*1e9,rel_wavelength 


def get_wavelength_old(color):
    """
    data["grating"] is lines per meter

    returns wavelength in meter
    """
    data = get_data_fromsearchkey(f"{color}_wave")
    y,x=data["maxima_dist"], data["dist"]
    g= data["grating"]
    return y/np.sqrt(x**2 + y**2)/g

def get_slope_limits(x,y,fraction=0.12):
    #where slope is smaller than some fraction of the min slope
    x_deriv,y_deriv = num_deriv(x,y,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,3)
    y_deriv = -y_deriv
    y_deriv -=np.max(y_deriv)*fraction
    limit = positive_x_limits(x,x_deriv,y_deriv,use_bumps=None)
    return limit

def get_taper_start_equil(x,y):
    #where second derivative spikes then goes to zero again
    x_deriv,y_deriv = num_deriv(x,y,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,3)
    x_deriv,y_deriv = num_deriv(x_deriv,y_deriv,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,3)
    y_deriv-=np.max(y_deriv)*0.5
    limit = positive_x_limits(x,x_deriv,y_deriv,use_bumps=0)

    index = np.argmax(y_deriv)
    #return x_deriv[index]
    return limit[0][0]

def get_equil_taper(x,y,d_y):
    taper_start= get_taper_start_equil(x,y)
    f = taper_start<=x
    y=y[f]
    d_y = d_y[f]
    x=x[f]

    model = ExponentialDecayModel()
    params = model.guess(y,x=x,min_index=0)
    result = model.fit(y, params, x=x, weights=1/d_y**2)

    c = result.params['offsety'].value
    d_c  = result.params['offsety'].stderr
    return c,d_c,result

def get_taper_start(x,y):
    #where second derivative spikes then goes to zero again
    x_deriv,y_deriv = num_deriv(x,y,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,3)
    x_deriv,y_deriv = num_deriv(x_deriv,y_deriv,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,3)
    y_deriv -=np.max(y_deriv)*0.2
    limit = positive_x_limits(x,x_deriv,y_deriv,use_bumps=0)
    return limit[0][1]



def lockin_voltage_to_current(lockin_voltage):
    """
    given Voltage in V from lockin returns flowing current in nA while laser is on
    """
    lockin_voltage = ensure_numpy_array(lockin_voltage)
    # *20 from lockin
    # *1e1 as photocell has a 1V per 1 nA conversion
    return lockin_voltage/20 *1e1

def avg_taper(x,y,d_y):
    taper_start= get_taper_start(x,y)
    f = taper_start<=x
    y=y[f]
    d_y = d_y[f]
    x=x[f]

    model = lmfit.models.ConstantModel()
    params = model.guess(y,x=x)
    
    result = model.fit(y, params, x=x, weights=1/d_y**2)
    c = result.params['c'].value
    d_c  = result.params['c'].stderr
    return c,d_c,result



def get_adjusted_data(x,y,d_y,):
    V_avg,d_V_avg,result = avg_taper(x,y,d_y)
    I_rev,d_I_rev = lockin_voltage_to_current([V_avg,d_V_avg])
    I_photo,d_I_photo = lockin_voltage_to_current([y,d_y])

    M_calc,d_M_calc = sym.M_func(I_photo,I_rev),sym.d_M_func(I_photo,d_I_photo,I_rev,d_I_rev)
    return M_calc,d_M_calc,(I_rev,d_I_rev/I_rev),result

def get_zero_cross_expo(x,y_adjust,d_x,d_y_adjust,color,settings=BaseSetting):
    if "limits" in settings:
        slope_limit=[settings["limits"][color]]
    else:
        slope_limit_low=get_slope_limits(x,y_adjust,fraction=settings["start_max_frac"] if "start_max_frac" in settings else 0.5)
        slope_limit_high=get_slope_limits(x,y_adjust,fraction=settings["end_max_frac"] if "end_max_frac" in settings else 0.5)
        slope_limit= [ (slope_limit_low[0][0],  slope_limit_high[-1][-1] )]

    f = inbetween_chain(x,slope_limit)
    pars,d_par,odr_output = odr_exponential_lin(x[f],y_adjust[f],d_x[f],d_y_adjust[f],False)

    zero_at = sym.z_func_expo_covar(pars)
    d_zero_at= sym.d_z_func_expo_covar(pars,odr_output.cov_beta)
    covar_m = odr_output.cov_beta
    if "print" in settings and settings["print"]:
        parameter1 =""
        parameter2=""
        for p,d_par in zip(pars,d_par):
            parameter1 += f" {p:.3e} , "
            parameter2 += f" {p:.3e} & "
        covar =f" AA,BB,CC: {np.sqrt(covar_m[0,0]):.2e}, {covar_m[1,1]:.2e} , {covar_m[2,2]:.2e}  ; AB, BC, AC: {covar_m[0,1]:.2e} , {covar_m[1,2]:.2e} , {covar_m[0,2]:.2e}"

        covar2 =f" {np.sqrt(covar_m[0,0]):.2e} & {covar_m[1,1]:.2e} & {covar_m[2,2]:.2e}  & {covar_m[0,1]:.2e} & {covar_m[1,2]:.2e} & {covar_m[0,2]:.2e}"
        print(f"color: {color} A,B,C:{parameter1}, covars: {covar} ")
        print(f" {color} & {parameter2} \\\\")
        print(f" {color} &  {covar2}\\\\")

    function = lambda x: exponential_lin_model_func(pars,x)
    return zero_at,d_zero_at,slope_limit,odr_output,f,function

def get_zero_cross_lin(x,y_adjust,d_x,d_y_adjust,color,settings=BaseSetting):
    if "limits" in settings:
        slope_limit=[settings["limits"][color]]
    else:
        slope_limit_low=get_slope_limits(x,y_adjust,fraction=settings["start_max_frac"] if "start_max_frac" in settings else 0.5)
        slope_limit_high=get_slope_limits(x,y_adjust,fraction=settings["end_max_frac"] if "end_max_frac" in settings else 0.5)
        slope_limit= [ (slope_limit_low[0][0],  slope_limit_high[-1][-1] )]
    f = inbetween_chain(x,slope_limit)

    par,d_par,odr_output = odr_lin_model(x[f],y_adjust[f],d_x[f],d_y_adjust[f],False)
    intercept,slope= par
    zero_at,d_zero_at = sym.z_func_lin(*par), sym.d_z_func_lin(intercept,d_par[0],slope,d_par[1])
    function = lambda x: lin_model_func(par,x)
    return zero_at,d_zero_at,slope_limit,odr_output,f,function


def get_zero_cross_of_color(color,settings=BaseSetting,extra_data=False):
    data = get_data_fromsearchkey(f"{color}_meas_extra")
    x = -data[0]
    d_x = data[1]
    y = data[2]
    d_y=data[3]
    M_calc,d_M_calc,I_reverse_data,fit_result = get_adjusted_data(x,y,d_y)

    zero_at,d_zero_at,slope_limit,odr_output,slope_filter,func = get_zero_cross_expo(x,M_calc,d_x,d_M_calc,color,settings=settings)
    rel_par = np.array([np.abs(d_/_) for _,d_ in zip(odr_output.beta,odr_output.sd_beta)])

    d_zero_at = sym.d_z_func_expo_covar(odr_output.beta,odr_output.cov_beta)
    if "linear" in settings and color in settings["linear"]:
        print("do linear")
        zero_at,d_zero_at,slope_limit,odr_output,slope_filter,func = get_zero_cross_lin(x,M_calc,d_x,d_M_calc,color,settings=settings)

    if extra_data:
        return zero_at,d_zero_at,slope_limit,odr_output,slope_filter,func
    return zero_at,d_zero_at



def get_millikan_data(color_list = color_list,settings=BaseSetting):
    x,d_x,y,d_y=list(),list(),list(),list()
    for color in color_list:
        freq, rel_freq = get_frequency(*get_wavelength(color))
        x.append(freq)
        d_x.append(rel_freq*freq)
        zero_at,d_zero_at = get_zero_cross_of_color(color, settings=settings )
        y.append(zero_at)
        d_y.append(d_zero_at)
    return np.array(x),np.array(y),np.array(d_x),np.array(d_y)

def get_milikan_lin_fit(settings=BaseSetting,color_list = color_list,show_plot=False):

    x,y,d_x,d_y=get_millikan_data(color_list=color_list,settings=settings)
    if "use_odr" in settings and not settings["use_odr"]:
        d_x=None
        d_y=None
    (c,slope),(d_c,d_slope),odr_output = odr_lin_model(x,y,d_x,d_y,without_output=False)
    if show_plot:
        plt.errorbar(x, y, xerr=d_x, yerr=d_y, fmt='o', label=f"adjusted datapoints ")
        plt.plot(x,lin_model_func(odr_output.beta,x),label="fitted")
        zero_at,d_zero_at = sym.z_func_lin(c,slope), sym.d_z_func_lin(c,d_c,slope,d_slope)
        plt.title(f" slope: {slope:.2e} +- {d_slope/slope*100:.2e} % \n intercept: {c:.2e} +- {np.abs(d_c/c)*100:.2e} % eV \n zero at: {zero_at:.2e} +- {d_zero_at/zero_at*100:.2e} %  \n work function : {ratio*zero_at:.2e} +- {d_zero_at/zero_at*100:.2e} %  eV")
        plt.xlabel("frequency [Hz]")
        plt.ylabel("stopping voltage [V]")
        plt.legend()
        plt.show()