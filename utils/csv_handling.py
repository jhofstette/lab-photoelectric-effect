import os
import numpy as np
import matplotlib.pyplot as plt

class NP_2D_DictWrapper:
    """
    intended to be used with numpy.genfromtxt, names=true
    overloads the [  ] operator to either be able to use headername to access a column
    or the column index
    to get row use get_row(index)
    get keys with keys()
    """
    def __init__(self, data):
        self.data = data
        self.keys()
    
    def get_row(self, index):
        return self.data[index]
    
    def column_count(self):
        return len(self.data.keys())
    
    def keys(self):
        return self.data.dtype.names

    def __getitem__(self, key):
        # Check if key is a string (header name)
        if isinstance(key, str) or isinstance(key, slice):
            return self.data[key]
        elif isinstance(key, int):
            return self.data[self.data.dtype.names[key]]
        else:
            raise TypeError("Key must be either a string (column name),an integer (column index) or a slice")


FOLDER_DATA_DEF = "csv_data"
FOLDER_FIT_DEF = "fit_plots"

def read_csv_to_dict(filename,FOLDER_DATA = FOLDER_DATA_DEF,header=True):
    """
    returns wrapped data from numpy genfromtxt with class :class:`NP_2D_DictWrapper`
    """
    if FOLDER_DATA is None:
        FOLDER_DATA = FOLDER_DATA_DEF
    path = os.path.join(FOLDER_DATA, filename)
    data = np.genfromtxt(path, delimiter=';', dtype=None, names=header, encoding=None)
    if header:
        data = NP_2D_DictWrapper(data)
    return data


def save_current_plot(name, FOLDER_FIT = FOLDER_FIT_DEF):
    if not os.path.exists(FOLDER_FIT):
            try:
                os.makedirs(FOLDER_FIT)
            except OSError as e:
                print(f"Error creating folder: {e}")
    filename = os.path.join(FOLDER_FIT, f"{name}.png")
    plt.savefig(filename)

def get_filename(par,FOLDER_DATA = FOLDER_DATA_DEF) :
    """
    tries to get a filename containing the keyword in the
    """
    data_list = os.listdir(FOLDER_DATA)
    if(isinstance(par, int)) :
        return data_list[par]
    else:
        for data in data_list:
            if par in data:
                return data
    return ""

def get_data_fromsearchkey(searchkey,header=True) :
    try:
        filename = get_filename(searchkey)
        result = read_csv_to_dict(filename,header=header)
        return result
    except Exception as e:
        print("Error:", e)

