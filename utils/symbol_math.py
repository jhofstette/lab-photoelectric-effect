from itertools import chain, combinations
import sympy as sp

class PartialFactoringSetting:
    def __init__(self, use_exponent_weight=False, put_into_fraction=False, ):
        self.use_exponent_weight = use_exponent_weight
        self.put_into_fraction= put_into_fraction

DEFAULT_PARTIALFACTORING_SETTING =  PartialFactoringSetting()
#main function
def partial_factoring(expr,setting=DEFAULT_PARTIALFACTORING_SETTING): 
    """
    given an expression and a PartialFactoringSetting instance, recursivly partially factors the expression
    by finding in sums common factors in a subset of terms and factoring that

    TODO atm doesnt check conditions, works under the assumptions that a^c * b^c = (a*b)^c and (a^b)^c = a^(b*c) always hold

    """

    if not isinstance(expr, sp.Expr):
        return expr
    if expr.is_Function and not isinstance(expr, sp.Pow):
        factored_args = [partial_factoring(arg,setting=setting ) for arg in expr.args]
        return expr.func(*factored_args)
    if setting.put_into_fraction:
        nom,denom = expr.as_numer_denom()
        if denom!=1:
            return partial_factoring(nom,setting=setting) / partial_factoring(denom,setting=setting) 
    if isinstance(expr, sp.Add):
        return partial_factoring_sum(expr,setting=setting)
    else:
        return factoring_of_product(expr,setting=setting)
     

def non_zero_powerset(iterable):
    """
    returns iterable over powersets of a given iterable, excluding the null set
    """
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1))

def sum_up_exponents_factor_list(factorlist): 
    """
    given a factorlist returns a factorlist with unique base and summed up exponents (dropping bases with exponents==0)
    """
    lookup_dict=dict()
    for element,number in factorlist:
        lookup_dict[element] = lookup_dict.get(element,0) + number
    return [(key, lookup_dict[key]) for key in lookup_dict.keys() if lookup_dict[key]!=0]

def inverse_factor_list(factorlist): 
    """ 
    given a factorlist, returns the factorlist of its reciprocal
    """
    return [(base,-exponent) for base,exponent in factorlist]


def get_flatten_factor_list(expr):
    """
    gven an expression returns a list result of (base,exponent) such that expr==product([e[0]**e[1] for e in result] )
    under the assumptions that a^c * b^c = (a*b)^c and (a^b)^c = a^(b*c) always hold
    """
    if isinstance(expr, sp.Mul):
        factor_list = []
        for factor in expr.as_ordered_factors():
            factor_list.extend(get_flatten_factor_list(factor))
    elif isinstance(expr, sp.Pow):
        base, outer_power = expr.args
        inner_factors = get_flatten_factor_list(base)
        factor_list = [(factor, power * outer_power) for factor, power in inner_factors]
    else:
        return [(expr, 1)]
    return sum_up_exponents_factor_list(factor_list)


def get_base_factors(expr):
    """
    returns list of base identifiers (tuples of (base, sig_exponent) ) and
    a dict of key=base identifiers, value = scalar_exponent  , scalar_exponent is positive

    for each base identifier,   base ** sig_exponent*scalar_exponent is a factor of the expression
    """
    factorlist = get_flatten_factor_list(expr)
    base_list = list()
    exp_scalar_dict = dict()
    for base,exponent in factorlist:
        if isinstance(base,sp.Add):
            #maybe better way, issue : different representation of base need to be transformed to the same one or some comparision
            base = sp.expand(base) 

        if isinstance(exponent, sp.Expr):
            constant_add, non_constant_list = exponent.as_coeff_add()
        else:
            constant_add, non_constant_list = exponent,[0]

        if constant_add!=0:
            const_exponent_sig= (base, 1 if constant_add>0 else -1 )
            base_list.append( const_exponent_sig)
            exp_scalar_dict[const_exponent_sig]= const_exponent_sig[1]*constant_add #=abs(constant_add)

        non_constant_term = sp.Add(*non_constant_list)
        non_constant_term = sp.expand(non_constant_term)
        for symbolic_term in non_constant_term.as_ordered_terms():
            if symbolic_term==0:
                break
            constant_scalar, non_constant_mul = symbolic_term.as_coeff_mul()
            constant_exp_scalar_sign = 1 if constant_scalar>0 else -1 

            new_base = (base,constant_exp_scalar_sign*sp.Mul(*non_constant_mul))
            base_list.append( new_base )
            exp_scalar_dict[new_base]= constant_exp_scalar_sign*constant_scalar #=abs(constant_add)


    return base_list,exp_scalar_dict


def update_subset_dict_and_values(index, base_set, subset_dict,best_subset_list,count_b):
    """
    given index of some value and a base_set based on that value iterate through powerset of base_set
    and note down in subset_dict the index of each element in the powerset
    count_b (gets returned) is the length of the longest array in the subset_dict, 
    best_subset_list contains all the keys to the values which have length count_b
    """
    for pow_set in non_zero_powerset(base_set):
            #mark down in dictionary the indexes for terms that contain powerset
            if pow_set in subset_dict:
                subset_dict[pow_set].append(index)
            else:
                subset_dict[pow_set]= [index]
                
            #now check if current factorset  
            count_for_powset = len(subset_dict[pow_set])
            if count_for_powset>count_b:
                best_subset_list.clear()
                best_subset_list.append(pow_set)
                count_b=count_for_powset
            elif count_for_powset==count_b:
                best_subset_list.append(pow_set)
    return count_b


def factorlist_together(factorlist):
    """
    returns sympy expression for a factorlist
    """
    return sp.Mul(*[e[0]**e[1] for e in factorlist])

def get_total_factorlist(partial_factorset,index_list_avail_terms,exp_scalar_dicts ):
    """
    given a partial factorset , index of terms whos factorset contains the partial factorset (index_list_avail_terms)
    and two lookup arrays where at index of term is set of all its factors (base_factorset) 
    and the exponents to those factors (factorset_exponents)

    then returns an optimal factorlist (all it does is figure out the optimal exponent to each factor in the partial factorset)
    aswell as a list with the optimal scalars it found
    """
    total_factorlist=list()
    ideal_scalar_list=list()
    for factor in partial_factorset:

        def get_factor_exponent_scalar(index):
            #gets factorset_exponent_dict from index, grabs exponent for given factor
            #scalar is positive
            return exp_scalar_dicts[index][factor]
        
        ideal_scalar = min(map(get_factor_exponent_scalar,index_list_avail_terms))
        factor_exponent = factor[1] * ideal_scalar

        total_factorlist.append( (factor[0],factor_exponent))
        ideal_scalar_list.append(ideal_scalar)
        

    return total_factorlist,ideal_scalar_list

def get_all_total_factor_lists(best_part_factorset_list, part_factorset_dict,exp_scalar_dicts): 
    """
    returns a list where each element is a 3-tuple, first is factorset, second is indexlist of terms containing the factorset
     and third is the totalfactorlist for that factorset
    """
    return [(e,part_factorset_dict[e], get_total_factorlist(e,part_factorset_dict[e],
                                                            exp_scalar_dicts,)) for e in best_part_factorset_list]


def iterative_partial_factoring_sum(expr, setting=DEFAULT_PARTIALFACTORING_SETTING): 
    """
    given an expression thought as a sum expression, figure out optimal subset of terms
    that share factors and returns factor, factored_out, leftover
    where expr = factor*factored_out + leftover
    """
    best_part_factorset_list = list()
    count_best=0
    base_factorsets=list()
    exp_scalar_dicts=list()
    part_factorset_dict= dict()


    term_list = expr.as_ordered_terms()

    for i,term in enumerate(term_list):

        factors,exp_scalar_dict = get_base_factors(term)
        base_factorsets.append(factors)
        exp_scalar_dicts.append(exp_scalar_dict)

        count_best= update_subset_dict_and_values(i, base_factorsets[i],
                                                        part_factorset_dict,best_part_factorset_list,count_best)

    #if true no two terms share factors
    if count_best<2:
        return 1,expr,0
    
    #figure out which base factor set to use
    def weight_element(element): 
        #element[2] is the result of get_total_factorlist each
        return sum(element[2][1])

    if setting.use_exponent_weight:
        all_factorlists = get_all_total_factor_lists(best_part_factorset_list,part_factorset_dict,
                                                 exp_scalar_dicts)

        choosen_baseset,index_list_avail_terms, get_total_factorlist_result =max(all_factorlists, key=weight_element)
        total_factorlist = get_total_factorlist_result[0]
    else:
        choosen_baseset = max(best_part_factorset_list, key=len)
        index_list_avail_terms = part_factorset_dict[choosen_baseset]
        total_factorlist,ideal_scalars = get_total_factorlist(choosen_baseset,index_list_avail_terms,
                                          exp_scalar_dicts)


    inv_total_factor_factorlist = inverse_factor_list(total_factorlist)
    factored_out,leftover=0,0

    for i, term in enumerate(term_list):
        if i in index_list_avail_terms:
            factor_list_result = list()
            for base_factor in base_factorsets[i]:
                base_expr = base_factor[0]  
                factor_list_result.append( (base_expr, base_factor[1]*exp_scalar_dicts[i][base_factor]))
            
            
            factor_list_result.extend(inv_total_factor_factorlist)

            simpl_term = factorlist_together(sum_up_exponents_factor_list(factor_list_result))
            factored_out += simpl_term
        else:
            leftover+= term

    return factorlist_together(total_factorlist),factored_out,leftover

def partial_factoring_sum(expr,setting=DEFAULT_PARTIALFACTORING_SETTING):
    """
    given expr applying func:`iterative_partial_factoring_sum` till nothing is left,
    then either applying the function again till no common factors can be found, then applying func`partial_factoring_sum`
    """
    start_factor, factored_part, leftover = iterative_partial_factoring_sum(expr, setting=setting)
    result = sp.Mul(start_factor, factored_part, evaluate=False) if start_factor!=1 else factored_part
    while leftover != 0:
        total_factor, factored_part, leftover = iterative_partial_factoring_sum(leftover, setting=setting)
        result += sp.Mul(total_factor, factored_part, evaluate=False) if total_factor!=1 else factored_part
    if start_factor==1:
        return factoring_of_sum_parts(result,setting=setting)
    else :
        return partial_factoring_sum(result,setting=setting)
    
def factoring_of_product_factorlist(factorlist,setting=DEFAULT_PARTIALFACTORING_SETTING):
    """
    given factorlist, iterates through it and applies func:`partial_factoring` to base and exponent
    """
    return [(partial_factoring(e[0],setting=setting),partial_factoring(e[1],setting=setting)) for e in factorlist]

def factoring_of_product(product,setting=DEFAULT_PARTIALFACTORING_SETTING):
    """
    given expression returns itself if its a symbol or number (cannot be simplified further)
    else apply func:`get_flatten_factor_list`  , func:`factoring_of_product_factorlist` and func: `factorlist_together` to the expression

    essentially simplifing factors of the expression
    """
    if product.is_symbol or product.is_number:
        return product
    return factorlist_together(factoring_of_product_factorlist(get_flatten_factor_list(product),setting=setting))

def factoring_of_sum_parts(expr,setting=DEFAULT_PARTIALFACTORING_SETTING):
    """
    given expression applying func:`partial_factoring` to its terms
    """
    result = 0 
    for term in expr.as_ordered_terms():
        result += partial_factoring(term,setting=setting)
    return result

        
