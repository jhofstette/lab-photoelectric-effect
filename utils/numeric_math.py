import numpy as np
import sympy as sp

import lmfit 
from scipy.interpolate import interp1d
from collections.abc import Iterable

from utils.csv_handling import save_current_plot

from scipy.odr import ODR, Model, RealData

def to_float(val):
    """
    tries to covnert to float, if it cant return value again
    """
    try:
        return float(val)
    except:
        return val
def ensure_numpy_array(arr):
    if arr is not None and not isinstance(arr, np.ndarray):
        arr = np.array(arr)
    return arr

def num_deriv(x,y,w=1):
    x = ensure_numpy_array(x)
    y = ensure_numpy_array(y)
    y_deriv = (y[w:]-y[:-w])/(x[w:]-x[:-w])
    x_deriv = (x[w:]+x[:-w])/2
    return x_deriv, y_deriv

def sliding_average(x,y,w=2):
    x = ensure_numpy_array(x)
    y = ensure_numpy_array(y)
    conv = np.ones(w)/w
    return np.convolve(x, conv, mode='valid'),np.convolve(y, conv, mode='valid')


def get_num_extrema(x,y,with_deriv=True, w_deriv=10, w_avg = 10):
    """
    returns viable,extremas,x_deriv,y_deriv
    viable indicates if the operation can even be done
    extremas is an array containing the zero crossings of the derivative
    if wtih_deriv true then it also returns x_deriv and y_deriv
    """

    if(len(x)<=w_deriv+ w_avg) :
        if with_deriv:
            return False,[],[],[]
        return False,[]
    x_deriv,y_deriv = num_deriv(x,y,w=w_deriv)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,w=w_avg)
    zero_crossings = interpolate_zero_crossings(x_deriv,y_deriv)
    if with_deriv :
        return True,zero_crossings,x_deriv,y_deriv
    return True,zero_crossings


def interpol_pos_fft(time,signal, set_first_zero=True):
    """
    returns only positive part of fft
    """
    if(len(time)<2) :
        return [],[]
    uniform_time = np.linspace(time[0], time[-1], len(time))
    interp_func = interp1d(time, signal)  
    uniform_signal = interp_func(uniform_time)  #interpolate for uniform spacing
    uniform_signal_fft = np.fft.fft(uniform_signal)
    freqs = np.fft.fftfreq(len(uniform_signal), d=(uniform_time[1] - uniform_time[0]))  
    zero_freq_idx = len(freqs) // 2
    if set_first_zero:
        uniform_signal_fft[0] =0  #zero frequency
    freqs, uniform_signal_fft = np.fft.fftshift(freqs),np.fft.fftshift(uniform_signal_fft)

    return freqs[zero_freq_idx:], uniform_signal_fft[zero_freq_idx:]

def interpolate_zero_crossings(x, y):
    # Find indices where y changes sign (zero crossings)
    zero_crossings_indices = np.where(np.diff(np.sign(y)))[0]
    
    # Interpolate to find x values where y crosses zero
    x_zero_crossings = []
    for idx in zero_crossings_indices:
        interp_func = interp1d([y[idx], y[idx + 1]], [x[idx], x[idx + 1]])
        x_zero_crossings.append(interp_func(0).item())
    
    return x_zero_crossings


def inbetween_filter(x,x_low,x_high):
    return (x_low<=x) & (x<= x_high)

def inbetween_chain(x,limits):
    result = np.full(len(x), False, dtype=bool)
    for limit in limits:
        result = result | inbetween_filter(x,limit[0],limit[1])
    return result

def positive_x_limits(x_ref,x_mod,y_mod,use_bumps=None):
    """
    returns a list of tuples of start and end of specified bumps in y_mod

    """
    x_ref = ensure_numpy_array(x_ref)
    x_mod = ensure_numpy_array(x_mod)
    y_mod = ensure_numpy_array(y_mod)
    x_zero_crossings = interpolate_zero_crossings(x_mod,y_mod)

    if y_mod[0]>0:
        x_zero_crossings.insert(0,x_ref[0])
    if y_mod[-1]>0:
        x_zero_crossings.append(x_ref[-1])
    
    if use_bumps is None:
        use_bumps= range(len(x_zero_crossings)//2)
    if not isinstance(use_bumps, Iterable):
        use_bumps = [use_bumps]

    limits = []
    for bump in use_bumps:
        if bump*2+1>=len(x_zero_crossings):
            break
        limits.append((x_zero_crossings[bump*2],x_zero_crossings[bump*2+1]))
    return limits

def positive_x_filter(x_ref,x_mod,y_mod,use_bumps=None):
    """
    returns a filter applicable to x_ref which is true when y_mod is positive and its a bump specified and false else
    each group where y_mod is positive is a bump, index starts at 0, 
    bumps to be included should be specified in an array in use_bumps, if None include all, if a number it gets wrapped in an array
    """
    limits = positive_x_limits(x_ref,x_mod,y_mod,use_bumps=use_bumps)
    return inbetween_chain(x_ref,limits)

def lin_model_func(beta, x):
    intercept,slope = beta
    return intercept + slope*x
def odr_lin_model(x,y,sx,sy,without_output=True):
    """
    parameters are in order intercept, slope
    """
    x = ensure_numpy_array(x)
    y = ensure_numpy_array(y)
    sx = ensure_numpy_array(sx)
    sy = ensure_numpy_array(sy)

    model = Model(lin_model_func)
    
    data = RealData(x, y, sx=sx, sy=sy)
    x_deriv,y_deriv = num_deriv(x,y,1)
    slope_guess = np.mean(y_deriv)
    
    initial_guess = [np.mean(y-slope_guess*x),slope_guess]
    odr = ODR(data, model, beta0=initial_guess,maxit=1000)

    output = odr.run()

    fitted_par = output.beta
    d_fitted_par = output.sd_beta
    if without_output:
        return fitted_par,d_fitted_par
    return fitted_par,d_fitted_par,output

def exponential_lin_model_func(beta, x):
    A, B, C = beta
    B = np.abs(B) + 1e-16
    return C - B**(x - A)
def odr_exponential_lin(x,y,sx,sy,without_output=True):
    model = Model(exponential_lin_model_func)
    
    data = RealData(x, y, sx=sx, sy=sy)

    initial_guess = [0,1.5,0]
    odr = ODR(data, model, beta0=initial_guess)

    output = odr.run()

    output.beta[1]= np.abs(output.beta[1])
    fitted_par = output.beta
    d_fitted_par = output.sd_beta

    if without_output:
        return fitted_par,d_fitted_par
    return fitted_par,d_fitted_par,output



class ExponentialDecayModel(lmfit.Model):
    """
    exponential decay model (exp(kx) with negative k) with scalar, offset in x and y
    get_min_index function gives back the index at which the given x and data should be started sliced 
    guess_values gives back guessed parameters 
    used keywords: offsety , min_index (uses it directly) , error, error_factor for deciding cutoff for decay guess
    data gets shifted and correctly flipped then log is taken
    log_data then is a negative slope tapering off to zero
    log(error*error_factor) is used as a min_value as a condition to get the slope itself without the taper
    value of that slope is the guessed decay
    """
    test = True
    def __init__(self, *args, **kwargs):
        def exponential(x, offsetx,offsety,scale,decay ):
            result = scale*np.exp(-(x-offsetx)*decay) + offsety
            return result
        
        super().__init__(exponential, *args, **kwargs)
    def get_min_index(self,data):
        return np.argmax(np.abs(data-np.mean(data)))
    
    def guess_values(self, data, **kwargs):
        min_index = kwargs["min_index"] if "min_index" in kwargs else self.get_min_index(data)
        data = data[min_index:]
        low_i, high_i = np.argmin(data), np.argmax(data)
        low,high = data[low_i], data[high_i]
        pos_scale = high_i<low_i

        offsety = kwargs["offsety"] if "offsety" in kwargs else low if pos_scale else high
        scale = (high-offsety if pos_scale else low-offsety)
        
        offsetx=0
        decay = 1
        if("x" in kwargs):
            x = kwargs["x"][min_index:]
            error = kwargs["error"] if "error" in kwargs else 1
            error_factor = kwargs["error_factor"] if "error_factor" in kwargs else 5

            data2 = np.sign(scale)*(data-offsety)

            min_data= np.min(data2)
            data2 = data2 -min_data *(min_data<0)+1
            
            offsetx = x[np.argmax(data2)]
            log_data = np.log(data2)

            cutoff_index = np.argmin(log_data>np.log(error*error_factor))
            decay = -(log_data[cutoff_index] - log_data[0])/(x[cutoff_index]-x[0]) if x[cutoff_index]-x[0]!=0 else 0
        return offsetx,offsety,scale,decay



    def guess(self, data, **kwargs):
        offsetx,offsety,scale,decay = self.guess_values( data, **kwargs)
        diff = np.max(data)-np.min(data)
        params = self.make_params(offsetx = offsetx,offsety = offsety, scale = scale, decay = decay)
        params["offsety"].set(min=offsety-diff/2, max=offsety + diff/2)
        params["offsetx"].set(vary=False)
        fraction_air = 0.3
        scale_min,scale_max = scale*(1-fraction_air), scale*(1+fraction_air)
        if(scale<0):
            scale_min,scale_max = scale_max,scale_min
        params["scale"].set(min=scale_min, max=scale_max)
        return params
    
import matplotlib.pyplot as plt

def get_equil_value(time,data,get_fit_dict =False,fit_name="", FOLDER_FIT = None,x_axis="time (s)", y_axis="readout") :
    """
    fits with model class :class:`ExponentialDecayModel`
    and returns the fits y offset

    if fit_name is not "" it will show and save a plot of the fit
    """
    model = ExponentialDecayModel()
    min_index = model.get_min_index(data)
    time,data = time[min_index:],data[min_index:]
    pars = model.guess(data,x=time,error = 1,error_factor = 10)

    result = model.fit(data,pars, x= time)

    if fit_name!="":
        plt.plot(time, data, 'bo', markersize=2,label='Data')
        plt.plot(time, result.init_fit, 'b-', label='initial fit')
        plt.plot(time, result.best_fit, 'r-', label='Fit')

        plt.xlabel(x_axis)
        plt.ylabel(y_axis)
        plt.title(f"data and fit of {fit_name}")
        plt.legend()
        if FOLDER_FIT:
            save_current_plot(fit_name, FOLDER_FIT = FOLDER_FIT)
        else:
            save_current_plot(fit_name)
        plt.show()
        plt.clf()

    if get_fit_dict:
        result_dict = dict()
        result_dict["result"] = result
        for key in result.params.keys():
            result_dict[key] = (result.params[key].value,result.params[key].stderr)
        return result_dict
    offsety_value = result.params['offsety'].value
    offsety_error = result.params['offsety'].stderr
    return offsety_value,offsety_error

def get_rel_error_symbol(symbol):
    return sp.Symbol(f"\\sigma_{{\\text{{rel}},{symbol}}}", real=True,positive=True)

def get_abs_error_symbol(symbol):
    return sp.Symbol(f"\\sigma_{{{symbol}}}",real=True,positive=True)

def get_covar_symbol(symbol1,symbol2):
    return sp.Symbol(f"\\sigma_{{{symbol1};{symbol2}}}", real=True)

def get_key_value_list(keys, dict_to_use,use_alternating = True ):
    """
    returns a list containing all keys and in the same order the values to the key in the dict
    either alternating or first keys, then values
    """
    result = list()
    if use_alternating :
        for key in keys:
            result.append(key)
            if key in dict_to_use:
                result.append(dict_to_use[key])
    else:
        result.extend(keys)
        for key in keys:
            if key in dict_to_use:
                result.append(dict_to_use[key])
    return result

def gaussian_error_propagation(func,func_symbol=None,deriv_depth=1,use_rel_errors=False, output_rel=False, constants=set(),
                               rel_error_symbol_func =get_rel_error_symbol, abs_error_symbol_func = get_abs_error_symbol ):
    
    """
    given a sympy function calculates the uncertainty of that function using gaussian error propagation

    returns a sympy function representing the uncertainty of it, 
    a dictionary of symbols for the uncertainty of different variables
    aswell as if a func_symbol was specified a symbol representing the result.

    recommended to use func:`get_key_value_list` with the error_symbols dict and a list with wished order of symbols
    to get a list and then use lambdify(list,resulting_expr)

    "use_rel_errors": using relative or absolute errors for the variables
    "output_rel":  return the relative or absolute error of the function
    "constants": a list of variables that can be seen as constants
    """
    # Get a list of all variables in the function
    vars = set(func.free_symbols) - set(constants)
    error_symbol_func = rel_error_symbol_func if use_rel_errors else abs_error_symbol_func

    error_symbols = dict()
    
    # Calculate the derivative of the function with respect to each variable
    squared_res = 0
    for var in vars:
        error_symbol = error_symbol_func(var)
        error_symbols[var] = error_symbol
        if use_rel_errors :
            error_symbol*=var

        error_from_var=0
        deriv = func
        for i in range(deriv_depth):
            deriv = sp.diff(deriv, var)

            clean = deriv/func if output_rel else deriv
            error_from_var += clean*error_symbol**(i+1)

        squared_res += error_from_var**2

    error_propagation_func = sp.sqrt(squared_res)


    if func_symbol:
        symbol = rel_error_symbol_func(func_symbol) if output_rel else abs_error_symbol_func(func_symbol)
        return error_propagation_func,error_symbols,symbol
    else:
        return error_propagation_func,error_symbols
    

def gaussian_error_covar_propagation(func,constants=set(), covar_symbol_func = get_covar_symbol  ):
    
    """
    given a sympy function calculates the uncertainty of that function using gaussian error propagation

    returns a sympy function representing the uncertainty of it, 
    and a dictionary of symbols for (co)variancesof the variables


    recommended to use func:`get_key_value_list` with the error_symbols dict and a list with wished order of symbols
    to get a list and then use lambdify(list,resulting_expr)

    "constants": a list of variables that can be seen as constants
    """
    # Get a list of all variables in the function
    vars = list(set(func.free_symbols) - set(constants))

    covar_symbols = dict()
    num_var = len(vars)

    squared_res = 0
    for i in range(num_var):
        for j in range(i,num_var):
            variance_symbol = covar_symbol_func(vars[i],vars[j])
            covar_symbols[(vars[i],vars[j])] = variance_symbol
            multiplier= 1 if i==j else 2
            covar_term = multiplier* sp.diff(func, vars[i]) * sp.diff(func, vars[j]) *variance_symbol
            squared_res +=  covar_term

    error_propagation_func = sp.sqrt(squared_res)

    return error_propagation_func,covar_symbols




def get_np_func_uncert(parameter_list, expr, uncert_expr, error_symbols,use_alternating = True):
    """
    returns lambdified numpy functions equivalent to the sympy expression and its uncertainty expression
    the order of parameters is given by the parameter_list, error_symbols is a dict with parameter to error_parameter link
    parameterlist for the uncertainty function is based on the parameterlist given, either alternating parameter - error_parameter
    or first parameters and then in the same order the error_parameters
    """
    custom_sqrt = lambda x: np.sqrt(np.abs(x))

    custom_module = {'sqrt': custom_sqrt, 'numpy': np}
    uncer_parameter_list = get_key_value_list(parameter_list,error_symbols,use_alternating=use_alternating)
    return sp.lambdify(parameter_list,expr, modules=custom_module), sp.lambdify(uncer_parameter_list,uncert_expr, modules=custom_module)

def get_np_func_uncert_using_variances(parameter_list, expr, uncert_expr,covar_vars, variance_symbols):
    """
    ! for uncertainty expression using covariance
    returns lambdified numpy functions equivalent to the sympy expression and its uncertainty expression
    the order of parameters is given by the parameter_list, error_symbols is a dict with parameter to error_parameter link
    parameterlist for the uncertainty function is based on the parameterlist given, either alternating parameter - error_parameter
    or first parameters and then in the same order the error_parameters
    """
    custom_module = { 'numpy': np}
    np_func_prep = sp.lambdify(parameter_list,expr, modules=custom_module)
    np_func = lambda pars: np_func_prep(*pars)

    uncer_parameter_list= parameter_list.copy()
    var_count = len(covar_vars)

    for i in range(var_count):
        for j in range(i,var_count):
            key1,key2 = (covar_vars[i],covar_vars[j]), (covar_vars[j],covar_vars[i])
            uncer_parameter_list.append(variance_symbols[key1] if key1 in variance_symbols else variance_symbols[key2])
    np_uncert_func_prep = sp.lambdify(uncer_parameter_list,uncert_expr, modules=custom_module)

    def np_uncert_func(pars,covar_matrix):
        pars=[ e for e in pars]
        for i in range(var_count):
            for j in range(i,var_count):
                pars.append(covar_matrix[i,j])

        return np_uncert_func_prep(*pars)
    
    return np_func,np_uncert_func