import matplotlib.pyplot as plt
from utils.csv_handling import *
from lab_utils import *


def plot_meas_extra(color,show=True):
    data = get_data_fromsearchkey(f"{color}_meas_extra")
    wavelength = get_wavelength_old(color)*1e9

    plt.plot(-data[0],data[2],label="lock in")
    plt.plot(-data[0],data[4]*20,label="direct readout*20")
    plt.title(f"{color} , wavelength: {wavelength} nm")
    plt.xlabel("reverse bias (V)")
    plt.ylabel("photocell output (V)")
    plt.legend()
    if show:
        plt.show()

def plot_meas_testing(color):
    data = get_data_fromsearchkey(f"{color}_meas_extra")
    wavelength = get_wavelength_old(color)*1e9
    x = -data[0]
    y = data[2]
    limit_slope=get_slope_limits(x,y)[-1]
    plt.plot(x,y,label="lock in")
    plt.axvline(x=limit_slope[0], color='r', linestyle='--')
    plt.axvline(x=limit_slope[1], color='r', linestyle='--')

    x_deriv,y_deriv = num_deriv(x,y,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,2)
    plt.plot(x_deriv,y_deriv,label="deriv")
    x_deriv,y_deriv = num_deriv(x_deriv,y_deriv ,1)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,2)
    plt.plot(x_deriv,y_deriv,label="deriv 2")

    plt.title(f"{color} , wavelength: {wavelength} nm")
    plt.xlabel("reverse bias (V)")
    plt.ylabel("photocell output (V)")
    plt.legend()
    plt.show()

def plot_with_defect(color,show=True):
    data = get_data_fromsearchkey(f"{color}_meas_extra")
    wavelength = get_wavelength_old(color)*1e9

    data_def = get_data_fromsearchkey(f"{color}_defect_meas")

    plt.plot(-data[0],data[2],label="lock in")
    plt.plot(-data[0],data[4]*20,label="direct readout*20")
    plt.plot(-data_def[0],data_def[2],label="lock in defect")
    plt.title(f"{color} , wavelength: {wavelength} nm, with defect one")
    plt.xlabel("reverse bias (V)")
    plt.ylabel("photocell output (V)")
    plt.legend()
    if show:
        plt.show()

def check_adjusted_photocell_data(color,settings=BaseSetting):
    data = get_data_fromsearchkey(f"{color}_meas_extra")
    calc_wave,rel_calc_wave = get_wavelength(color)
    x = -data[0]
    d_x = data[1]
    y = data[2]
    d_y=data[3]
    M_calc,d_M_calc,I_reverse_data,fit_result = get_adjusted_data(x,y,d_y)
    zero_at,d_zero_at,slope_limit,odr_output,slope_filter,func = get_zero_cross_of_color(color,settings=settings,extra_data=True)


    f=slope_filter
    plot_limits(slope_limit)
    plt.plot(x[f],func(x[f]),label="fitted")
    plt.scatter(x[f], M_calc[f],label="data fitted",s=20)

    plt.errorbar(x, M_calc, xerr=d_x,yerr=d_M_calc, fmt='o', label='Data', markersize=3)
    plt.xlabel('reverse bias')
    plt.ylabel('$\sqrt{I_{photo}- I_{rev}} [\sqrt{nA} ]$')
    plt.title(f"{color} wavelength {calc_wave:.3e} +- {rel_calc_wave*100:.3e} % nm\n reverse current in nA: {I_reverse_data[0]:.3e} +- {I_reverse_data[1]*100:.3e} % \n zero at {zero_at:.3e} +- {d_zero_at:.3e} V")
    plt.legend()
    plt.show()

def get_intensity_comparision():
    intensity=list()
    for color in color_list:
        data = get_data_fromsearchkey(f"{color}_meas_extra")
        intensity.append(  interp1d(-data[0], data[2])(0))
        
    intensity = np.array(intensity)
    intensity = intensity / np.max(intensity)
    for color,i in zip(color_list,intensity):
        print(f"{color} &   {i:.2e}")

def check_reverse_current_fit(color):
    data = get_data_fromsearchkey(f"{color}_meas_extra")
    calc_wave,rel_calc_wave = get_wavelength(color)
    x = -data[0]
    y = data[2]
    d_y=data[3]
    M_calc,d_M_calc,I_reverse_data,fit_result = get_adjusted_data(x,y,d_y)
    plt.errorbar(x, y, yerr=d_y, fmt='o', label="Data")
    plt.plot(-data[0],data[4]*20,label="direct readout*20")
    plt.axvline(x=get_taper_start(x,y), color='r', linestyle='--')
    plt.plot(fit_result.userkws['x'], fit_result.best_fit,label="fit")
    plt.title(f"{color} wavelength: {calc_wave:.3e} +- {rel_calc_wave*100:.3e} % nm ")
    plt.xlabel("reverse bias (V)")
    plt.ylabel("photocell output (V)")
    plt.legend()
    plt.show()

def plot_limits(limits):
    for limit in limits:
        line = plt.axvline(x=limit[0], linestyle='--')
        plt.axvline(x=limit[1], color=line.get_color(), linestyle='--')
