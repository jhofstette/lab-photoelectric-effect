from lab_utils import *
from data_display import *

slope_expected = 4.125* 1e-15
color_list = ["yellow","green","blue"]
settings = {
    "limits":{
        "red":(-3,3),
        "yellow":(-3,3),
        "green":(-3,3),
        "blue":(-3,3),
        }
    }
get_milikan_lin_fit(settings=settings,color_list=color_list,show_plot=True)

