
from data_display import *

colors=["blue","green","yellow","red"]
settings_middle = {
    "limits":{
        "red":(-3,0.5),
        "yellow":(-3,0.9),
        "green":(-1.4,0.9),
        "blue":(-1.4,1.3),
        },
    }

settings_tight = {
    "limits":{
        "red":(-1.7,0.5),
        "yellow":(-1.6,0.9),
        "green":(-1.4,0.9),
        "blue":(-1.4,1.3),
        }
    }

settings_whole = {
    "limits":{
        "red":(-3,0.2),
        "yellow":(-3,0.8),
        "green":(-3,0.9),
        "blue":(-3,1.3),
        }
    }

settings_frac = {
    "start_max_frac": 0.3,
    "end_max_frac" :0.5,
}


setting_in_use= settings_tight
for color in colors:
    #check_reverse_current_fit(color)
    #check_adjusted_photocell_data(color,settings=setting_in_use)
    pass
get_intensity_comparision()
setting_in_use["print"] =True
get_milikan_lin_fit(settings=setting_in_use,color_list=colors,show_plot=True)
