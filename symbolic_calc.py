from utils.numeric_math import *
from utils.symbol_math import *
import sympy as sp

I_rev = sp.Symbol(f"I_{{rev}}")
I_p = sp.Symbol(f"I_{{photo}}")

M = sp.Symbol(f"M")
M_symb= sp.sqrt(2*sp.sqrt((I_p-I_rev)**2))

parameter_list_M = [I_p,I_rev]

d_uncert_M, error_symbols_M, M_uncert = gaussian_error_propagation(M_symb,M,use_rel_errors=False,output_rel=False)

d_uncert_M = partial_factoring(d_uncert_M)
###############################################


y_symb = sp.Symbol(f"y_{{maxima diff}}")
x_symb = sp.Symbol(f"x_{{dist}}")
g_symb = sp.Symbol(f"g_{{grating}}")

wavelength = sp.Symbol(f"\lambda")

W_symb = y_symb/sp.sqrt(x_symb**2 + y_symb**2)/g_symb

parameter_list_W = [y_symb,x_symb,g_symb]
rel_uncert_W, error_symbols_W, W_uncert = gaussian_error_propagation(W_symb,wavelength,constants=[g_symb],use_rel_errors=False,output_rel=True)

rel_uncert_W = sp.simplify(rel_uncert_W)
##################################
A_symb = sp.Symbol(f"A")
B_symb = sp.Symbol(f"B")
C_symb = sp.Symbol(f"C")
zero_symb = sp.Symbol(f"0")
zero_expo= A_symb+sp.log(C_symb)/sp.log(B_symb)
parameter_list_z = [A_symb,B_symb,C_symb]
d_uncert_z, error_symbols_z, z_uncert = gaussian_error_propagation(zero_expo,zero_symb,deriv_depth=1,use_rel_errors=True,output_rel=False)

d_uncert_covar_z, covar_symbols_z = gaussian_error_covar_propagation(zero_expo)

d_uncert_z = partial_factoring(d_uncert_z)
d_uncert_z = partial_factoring(d_uncert_z)

d_uncert_covar_z = partial_factoring(d_uncert_covar_z)
#d_uncert_covar_z = partial_factoring(d_uncert_covar_z)


####################################
m = sp.Symbol(f"m")
c = sp.Symbol(f"c")
parameter_list_z_lin = [c,m]
zero_lin = -c/m
d_uncert_z_lin, error_symbols_z_lin, z_uncert_lin = gaussian_error_propagation(zero_lin,zero_symb,deriv_depth=1,use_rel_errors=False,output_rel=False)
###########

M_func,d_M_func = get_np_func_uncert(parameter_list_M,M_symb,d_uncert_M,error_symbols_M)
W_func,rel_W_func = get_np_func_uncert(parameter_list_W,W_symb,rel_uncert_W,error_symbols_W)

z_func_expo,d_z_func_expo = get_np_func_uncert(parameter_list_z,zero_expo,d_uncert_z,error_symbols_z)
z_func_lin,d_z_func_lin = get_np_func_uncert(parameter_list_z_lin,zero_lin,d_uncert_z_lin,error_symbols_z_lin)

z_func_expo_covar,d_z_func_expo_covar = get_np_func_uncert_using_variances(parameter_list_z, zero_expo, d_uncert_covar_z,parameter_list_z, covar_symbols_z )
print("finished loading function")

if __name__ == "__main__":
    print("latex for M: ")
    print(sp.latex(M_symb))
    print(f"latex for M absolute uncertainty: {sp.latex(M_uncert)} ")
    print(sp.latex(d_uncert_M))

    print("latex for wavelength: ")
    print(sp.latex(W_symb))
    print(f"latex for wavelength rel uncertainty: {sp.latex(W_uncert)} ")
    print(sp.latex(rel_uncert_W))

    print("latex for zero exponent: ")
    print(sp.latex(zero_expo))
    print("latex for zero exponent absolute uncertainty: ")
    print(sp.latex(d_uncert_z))
    print()
    print("latex for zero exponent absolute uncertainty using covarianca: ")
    print(sp.latex(d_uncert_covar_z))



